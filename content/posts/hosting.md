---
title: "Hosting"
date: "30/01/2024"
---
I wasn't really sure what to post about, so I thought explaining how this blog works would be a good starting point.

This blog is created using the [Hugo](https://gohugo.io/) static site generator (SSG). If you don't know what an SSG is, it's basically a program that takes some content, such as Markdown files, and converts them into a static website - For my usecase, I have a Markdown file for each post, and Hugo converts each one into its own page on the site.

The posts in question are hosted over on [GitLab](https://gitlab.com/cute-catgirl/blog) along with the site configuration and whatnot.

Finally, I use [Netlify](https://netlify.com) to host the site - it automatically detects when I add a new post, rebuilds the site, and publishes it.

With this setup, I can write and publish posts on any device, and they'll appear on the blog almost immediately. I could definitely go further and build my own site from sctatch, self-host it, etc., but there's not much point right now. What I've got is simple, but it works, and that's the most important thing. It allows me to focus on the actual content of the site rather than playing around with every little detail.