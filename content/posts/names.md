---
title: "Names"
date: "06/03/2024"
---
Names are hard.
I don't know what my name is, or what it's going to be.
I've tried every one I can think of, but they all feel off somehow.

I don't have a big takeaway or conclusion to this post.
But I needed to write it down anyways.