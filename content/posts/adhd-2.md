---
title: "ADHD 2"
date: "05/06/2024"
---
I live inside my head a lot. I can’t remember the last time I wasn’t thinking about *something*. Even when I’m doing something that requires focus - coding, playing a video game, or writing an essay - my mind is constantly going off on little tangents. Some idea will pop up in my head and suddenly I’m contemplating the meaning of life, or designing a better text editor, or planning a conversation that may or may not happen two weeks from now. This has its benefits, of course. I’m great at noticing tiny details in things that others miss. I’m great at planning things ahead of time, avoiding shooting myself in the foot later on. But it also has some pretty big downsides.

It is *ridiculously* hard for me to actually get my thoughts out of my head and into something tangible. I’ve rewritten this paragraph about 9 times, trying to somehow express my thoughts in words. I have so many ideas and thoughts that I can’t filter out the ones that are actually good or relevant. My brain hurts often because I've got so many things in my head all at once. I take at least thrice as long to get a simple task done and I've been late on pretty much every single school assignment.

I don't know what to do.